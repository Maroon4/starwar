export default class SwapiService {

    _apiBase = 'https://swapi.co/api';
    _imageBase = 'https://starwars-visualguide.com/assets/img';

    getResource = async (url) => {
        const res = await fetch(`${this._apiBase}${url}`);

        if (!res.ok) {
            throw new Error(`Could not fetch ${url}` +
                `, received ${res.status}`)
        }
        return await res.json();
    };


    getAllPeople = async () => {
        const res = await this.getResource(`/people/`);
        return res.results.map(this._getPeopleProps);
    };

    getPeople = async (id) => {
        const people = await this.getResource(`/people/${id}/`);
        // console.log(JSON.stringify(people, null, 3));
        return this._getPeopleProps(people);

    };

    getAllPlanets = async () => {
        const res = await this.getResource(`/planets/`);
        return res.results.map(this._getPlanetProps);
    };

    getPlanet = async (id) => {

      const planet = await  this.getResource(`/planets/${id}/`);
       return this._getPlanetProps(planet);
       // console.log({planet});
    };

    getAllStarships =  async () => {
        const res = await this.getResource(`/starships/`);
        return res.results.map(this._getStarshipProps);

    };

    getStarship = async (id) => {
        const starship = await this.getResource(`/starships/${id}/`);
        return this._getStarshipProps(starship);
        // alert(JSON.stringify(starships, null, 3));
    };

    getPeopleImage = ({id}) => {
        return `${this._imageBase}/characters/${id}.jpg`
    };

    getStarshipImage = ({id}) => {
        return `${this._imageBase}/starships/${id}.jpg`
    };

    getPlanetImage = ({id}) => {
        return `${this._imageBase}/planets/${id}.jpg`
    };

    _extractId = (item) => {
        const idxReg =/\/([0-9]*)\/$/;
        return item.url.match(idxReg)[1];
    };

    _getPlanetProps = (planet) => {

        return {
            id: this._extractId(planet),
            name: planet.name,
            population: planet.population,
            rotationPeriod: planet.rotation_eriod,
            diameter: planet.diameter

            };

    };

    _getPeopleProps = (people) => {

        return {
            id: this._extractId(people),
            name: people.name,
            birthYear: people.birth_year,
            gendel: people.gendel

        }
    };

    _getStarshipProps = (starship) => {
        // console.log(starship);
        return {
            id: this._extractId(starship),
            name: starship.name,
            model: starship.model,
            passenger: starship.passenger,
            consumables: starship.consumables,
            costInCredits: starship.cost_in_credits
        }
    };

}

// const swapi = new SwapiService();
//
// swapi.getAllPeople().then((body) => {
//     console.log(body)
// });


