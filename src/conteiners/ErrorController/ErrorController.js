import  React, {Component} from 'react'
import Error from "../Errors";

export default class ErrorController extends Component {

    state = {
        hasError: false
    };

    componentDidCatch() {
        // console.log('componentDidCatch(error, errorInfo)');
        this.setState({hasError: true})
    }
    render() {
        if (this.state.hasError) {
            return <Error/>
        }
        return (
            this.props.children
        );
    }
}
