
import React from "react";
import hocHelper from "../HocHelper/HocHelper";
import SwapiService from "../../swapiService/SwapiService";

import "./itemList.css"


const ItemList = (props) => {

        const {data, onItemSelected, children: renderLabel} = props;

        const items = data.map((item) => {

            const {id} = item;

            const label = renderLabel(item);

            return (
                <li className="list-group-item"
                    key={id}
                    onClick={() => onItemSelected(id)}>
                    {label}
                </li>
            );
        });

        return (
            <ul className="item-list list-group">
            {items}
            </ul>
        );

        ItemList.defaultProps = {
          onItemSelected: () => {}
        };

};

const { getAllPeople } = new SwapiService;

export  default hocHelper(ItemList, getAllPeople);