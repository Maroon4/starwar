import React, { Component } from 'react';


import SwapiService from "../../swapiService/SwapiService";
import Spinner from "../Spinner";
import Error from "../Errors";

import './randomPlanet.css';

export default class RandomPlanet extends Component {

    static defaultProps = {
        updateInterval: 10000
    };

    static propTypes = {
        updateInterval: (props, propName, componentName) => {
            const value = props[propName];

            if (typeof value === 'number' && !isNaN(value)) {
                return null;
            }
            return new TypeError(`${componentName}: ${propName} must be number`);
        }
    };

    swapiService = new SwapiService();

    state = {
        planet: {},
        loading: true,
        error: false,

        // id: null,
        // name: null,
        // population: null,
        // rotationPeriod: null,
        // diameter: null

    };

    componentDidMount() {
        const {updateInterval} = this.props;
        this.updatePlanet();
        this.interval = setInterval(this.updatePlanet, updateInterval);

    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    onPlanetLoaded = (planet) => {
        this.setState({
            planet,
            loading: false,
            error: false
        });
    };

    onError = (err) => {
       this.setState({
           error: true,
           loading: false
       })
    };


    updatePlanet = () => {
        // console.log("update");
        const id = Math.floor(Math.random()*20) + 2;
        // const id = 1000000000;
        this.swapiService
            .getPlanet(id)
            .then(this.onPlanetLoaded)
            .catch(this.onError);
    };

    render() {

       const {planet, loading, error} = this.state;

       const allOk = !(loading || error);

       const errorActive = error ? <Error/> : null;
       const  spinner = loading ? <Spinner/> : null;
       const content = allOk ? <PlanetView planet={planet}/> : null;


        return (

               <div className="random-planet jumbotron rounded">
                {errorActive}
                {spinner}
                {content}
               </div>

        );


    }
}

const PlanetView = ({planet}) =>  {

    const {id, name, population,
        rotationPeriod, diameter } = planet;

    return (
        <React.Fragment>
            <img className="planet-image"
                 src={`https://starwars-visualguide.com/assets/img/planets/${id}.jpg`} />
            <div>
                <h4>{name}</h4>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item">
                        <span className="term">Population</span>
                        <span>{population}</span>
                    </li>
                    <li className="list-group-item">
                        <span className="term">Rotation Period</span>
                        <span>{rotationPeriod}</span>
                    </li>
                    <li className="list-group-item">
                        <span className="term">Diameter</span>
                        <span>{diameter}</span>
                    </li>
                </ul>
            </div>
        </React.Fragment>
    )

};
