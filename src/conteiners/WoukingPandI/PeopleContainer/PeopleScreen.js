import  React, {Component} from 'react';
import PeopleItemList from "../PeopleItemList";
import People from "../People";
import Error from "../../Errors";
import SwapiService from "../../../swapiService/SwapiService";

export  default class PeopleScreen extends Component{

    swapi = new SwapiService();

    state = {

        selectedPerson: 3,
        hasError: false

    };

    componentDidCatch(error, errorInfo) {
        // console.log('componentDidCatch(error, errorInfo)');
        this.setState({hasError: true})
    }

    onPersonSelected = (selectedPerson) => {
        this.setState({
            selectedPerson: selectedPerson
        })
    };

    render() {

        if (this.state.hasError) {
            return <Error />;
        }

        return (
            <div className="row mb2">
                <div className="col-md-6">
                    <PeopleItemList
                        onItemSelected={this.onPersonSelected}
                        getSwapi={this.swapi.getAllPeople}

                        // renderItems={({name, birthYear, gender}) => `${name} (${birthYear}, ${gender})` }
                    >
                        {({name, gender, birthYear}) => `${name} (${gender} , ${birthYear})`}
                    </PeopleItemList>
                </div>
                <div className="col-md-6">
                    <People peopleId={this.state.selectedPerson}/>
                </div>
            </div>
        );
    }
}