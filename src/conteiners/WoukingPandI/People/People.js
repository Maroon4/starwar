import React, {Component} from "react";
import SwapiService from "../../../swapiService/SwapiService";
import "./people.css"
import Spinner from "../../Spinner";
import ErrorButton from "../../ErrorButton";

export default class People extends Component{

    swapi = new SwapiService();

    state = {
        people: null,
        loading: true

    };

    componentDidMount() {
        this.updatePeople();
        // this.interal = setInterval(this.updatePeople, 4000);
    }

    componentDidUpdate(prevProps) {
        if (this.props.peopleId !== prevProps.peopleId) {
            this.updatePeople();
            // this.interal = setInterval(this.updatePeople, 4000);
            // clearInterval(this.interal);

        }
    }



    // componentWillUnmount() {
    //     // clearInterval(this.interal);
    // }

    onPeopleLoaging = (people) => {
        this.setState({
            people,
            loading: false

        })
    };

    updatePeople() {
        const {peopleId} = this.props;
        // console.log(JSON.stringify(peopleId, null, 3));

        if (!peopleId) {
            return;
        }

        this.swapi
            .getPeople(peopleId)
            .then(this.onPeopleLoaging
                // this.setState({
                // people: people

                // });
            )
    };



    render() {

        const {people,loading} = this.state;

        if(!this.state.people) {
            return <Spinner/>
        }

        const showResult = loading ? <Spinner/> : null;
        const content = !loading ? <PeopleView people={people}/> : null;


        // console.log(JSON.stringify({name}, null, 3));


        return(

            <div className="person-details card">
                {showResult}
                {content}
            </div>


        );

    }

}

const PeopleView = ({people}) => {

    const {id, name, gender, birthYear} = people;

    return (
        <React.Fragment>
            <img className="person-image"
                 src={`https://starwars-visualguide.com/assets/img/characters/${id}.jpg`} />

            <div className="card-body">
                <h4>{name}</h4>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item">
                        <span className="term">Gender</span>
                        <span>{gender}</span>
                    </li>
                    <li className="list-group-item">
                        <span className="term">Birth Year</span>
                        <span>{birthYear}</span>
                    </li>
                </ul>
                <ErrorButton/>
            </div>
        </React.Fragment>
    );
};