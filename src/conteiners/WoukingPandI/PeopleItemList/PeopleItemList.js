import React, {Component} from "react";

import SwapiService from "../../../swapiService/SwapiService";
import Spinner from '../../Spinner'
import whithData from "../whithData";

import "./PeopleItemList.css"




const PeopleItemList = (props) => {

    const { data, onItemSelected, children: renderLabel} = props;

        const items = data.map((item) => {

            const {id} = item;

            const label = renderLabel(item);

            return (
                <li className="list-group-item"
                    key={id}
                    onClick={() => onItemSelected(id)}>
                    {label}
                </li>
            );
        });


        return (
            <ul className="item-list list-group">
                {items}
            </ul>
        );

};

const {getAllPeople} = new SwapiService();
export default whithData(PeopleItemList, getAllPeople);