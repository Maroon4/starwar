import React, {Component} from 'react'
import Spinner from "../../Spinner";

const whithData = (View, getData) => {

    return class extends Component{
        state = {
            data: null
        };

        componentDidMount() {
            // this.onItemList();
            // const  {getSwapi} = this.props;

            getData()
                .then((data) => {
                    this.setState({
                       data
                    });
                });
        }
        //
        // onItemList = () => {
        //    const {getSwapi} = this.props;
        //
        //    getSwapi()
        //       .then((itemList) => {
        //       this.setState({
        //           itemList
        //       });
        //       // console.log(JSON.stringify(peopleList, null, 3));
        //   })
        // };

        render() {

            const {data} = this.state;

            if (!data) {
                return <Spinner/>;
            }

            return <View {...this.props} data={data}/>

        }
    }
};

export default whithData;