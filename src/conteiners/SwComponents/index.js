import {
    PersonList,
    PlanetList,
    StarshipList
} from './ItemLists'

import PeopleDetails from "./PeopleDetails";
import PlanetDetails from './PlanetDetails'
import StarshipDetails from './StarshipDetails'

export {
    PersonList,
    PlanetList,
    StarshipList,
    PeopleDetails,
    PlanetDetails,
    StarshipDetails
}