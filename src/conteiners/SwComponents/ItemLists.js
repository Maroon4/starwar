import React from 'react'
import ItemList from "../ItemList/ItemList";
import {hocHelper, withSwapiService, withChildFunction, composeF} from "../HocHelper"
import SwapiService from "../../swapiService/SwapiService";



// const swapi = new SwapiService();

// const {
//     getAllPeople,
//     getAllPlanets,
//     getAllStarships
// } = swapi;

const renderName = ({name}) => <span>{name}</span>;
const renderNameModel = ({name, model}) => <span>{name} ({model})</span>;

const mapPeopleMethodsToProps = (swapi) => {
    return {
        getData: swapi.getAllPeople
    }
};

const mapPlanetMethodsToProps = (swapi) => {
    return {
        getData: swapi.getAllPlanets
    }
};

const mapStarshipMethodsToProps = (swapi) => {
    return {
        getData: swapi.getAllStarships
    }
};

// const getData = {
//     mapPeopleMethodsToProps,
//     mapPlanetMethodsToProps,
//     mapStarshipMethodsToProps
//
// };



const  PersonList = withSwapiService(mapPeopleMethodsToProps)(
                      hocHelper(
                           withChildFunction(renderName)(
                               ItemList)));

const  PlanetList = composeF(
                    withSwapiService(mapPlanetMethodsToProps),
                    hocHelper,
                    withChildFunction(renderName)
                    )(ItemList);


const  StarshipList = composeF(
                      withSwapiService(mapStarshipMethodsToProps),
                      hocHelper,
                      withChildFunction(renderNameModel)
                      )(ItemList);

export {
    PersonList,
    PlanetList,
    StarshipList
}