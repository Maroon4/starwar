import React from 'react'
import ItemDetails, {Record} from "../ItemDetalies/ItemDetails";

import {withSwapiService} from "../HocHelper";


const  PeopleDetails = (props) => {
    return (
                  <ItemDetails {...props} >
                      <Record field = "gender" label = "Gender" />
                      <Record field = "eyeColor" label = "Eye Color" />

                  </ItemDetails>
    )
};

const  mapMethodToProps = (swapi) => {
  return {
      getData: swapi.getPeople,
      getImage: swapi.getPeopleImage
  }
};

export default withSwapiService(mapMethodToProps)(PeopleDetails);
