import React from 'react'
import ItemDetails, {Record} from "../ItemDetalies/ItemDetails";

import withSwapiService from "../HocHelper/whithSwapiService";

// const swapi = new SwapiService();

// const {getPeople,
//        getPlanet,
//        getStarship,
//        getPeopleImage,
//        getPlanetImage,
//        getStarshipImage} = swapi;


const  StarshipDetails = (props) => {

              return (
                  <ItemDetails
                      {...props}
                  >
                      <Record field = "model" label = "Model" />
                      <Record field = "consumables" label = "Consumables" />
                      <Record field = "costInCredits" label = "Cost" />
                  </ItemDetails>
              );
};

const mapMethodsToProps = (swapi) => {
     return {
         getData: swapi.getStarship,
         getImage: swapi.getStarshipImage
     }
};

export default withSwapiService(mapMethodsToProps)(StarshipDetails);