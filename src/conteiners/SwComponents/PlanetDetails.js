import React from 'react'
import ItemDetails, {Record} from "../ItemDetalies/ItemDetails";

import withSwapiService from "../HocHelper/whithSwapiService";



const  PlanetDetails = (props) => {



            return (
                <ItemDetails {...props}
                >
                    <Record field = "model" label = "Model" />
                    <Record field = "population" label = "Population" />
                    <Record field = "diameter" label = "Diameter" />
                </ItemDetails>
            );
          };


const mapMethodsToProps = (swapi) => {
    return {
        getData: swapi.getPlanet,
        getImage: swapi.getPlanetImage

    }
};

export default withSwapiService(mapMethodsToProps)(PlanetDetails);