import  React from 'react'

const composeF = (...funcs) => (comp) => {

    return funcs.reduceRight((previousValue, f) => {
        return f(previousValue)
    }, comp) ;

};

export default composeF;