import React, {Component} from 'react';
import Spinner from "../Spinner";
import Error from "../Errors";


const  hocHelper = (View) => {

    return class extends Component{

        state = {
            data: null,
            loading: true,
            error: false
        };

        componentDidUpdate(prevProps) {
            if (this.props.getData !== prevProps.getData) {
                this.updateData()
            }
        }

        componentDidMount() {
            this.updateData();
        }



        updateData () {
            this.setState({
                loading: true,
                error: false
            });

            console.log(JSON.stringify(this.props, null,3), '!!!!!!!!!!!!!');

            // const {getData} = this.props;

            this.props.getData()
                .then((data) => {
                    this.setState({
                        data,
                        loading: false
                    });
                    // console.log(JSON.stringify(data, null,3), '!!!!!!!!!!!!!')
                })
                .catch(() => {
                    this.setState({
                        error: true,
                        loading: false
                    });
                });
        }

        render() {

            const {data, error, loading} = this.state;
            // console.log(JSON.stringify(peopleList, null, 3));

            if (loading) {
                return <Spinner/>
            }

            if (error) {
                return <Error/>;
            }

            // const {children} = this.props;

            return <View {...this.props} data={data}/>
        }
    };

};

export default hocHelper;