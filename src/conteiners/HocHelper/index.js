import hocHelper from "./HocHelper";
import withSwapiService from "./whithSwapiService";
import withChildFunction from "./whithChildFunction";
import composeF from "./composeF";

export {
    withSwapiService,
    hocHelper,
    withChildFunction,
    composeF};
