import React, {Component} from 'react';
import Error from "../Errors";
import './errorButton.css'

export default class ErrorButton extends  Component{

    state = {
        trouble:false
    };

    render() {

        if (this.state.trouble) {
            return this.foo.bar = 0;
        }
        return(

                <button
                    className="error-button btn btn-danger btn-lg"
                    onClick={ () => this.setState({trouble: true})}
                >Trouble Generetor</button>

        );
    }

}
