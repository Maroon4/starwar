import React from 'react';
import SwapiService from "../../swapiService/SwapiService";

const swapi = new SwapiService();

const {
    Provider : SwapiProvider,
    Consumer : SwapiConsumer
} = React.createContext({swapi});

export {
    SwapiProvider,
    SwapiConsumer

};