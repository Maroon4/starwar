import {
        SwapiProvider,
        SwapiConsumer} from "./SwapiServiceContext";

export {
        SwapiConsumer,
        SwapiProvider};