import React, {Component} from "react";
import SwapiService from "../../swapiService/SwapiService";
import "./item-style.css"
import Spinner from "../Spinner";
import ErrorButton from "../ErrorButton";
import ErrorController from "../ErrorController";


const Record = ({item, field, label}) => {
     return (
         <li className="list-group-item">
             <span className="term">{label}</span>
             <span>{item[field]}</span>
         </li>
 )
};

export {
    Record
};

// const ItemView = ({item, image, children}) => {
//
//     // console.log(JSON.stringify(image, null, 3));
//
//     const {name} = item;
//
//
//     return (
//         <React.Fragment>
//             <img className="item-image"
//                  src={image}
//                  alt="item"/>
//
//             <div className="card-body">
//                 <h4>{name}</h4>
//                 <ul className="list-group list-group-flush">
//
//                     {
//                         React.Children.map(children, (child) => {
//                             return React.cloneElement(child, { item });
//                         })
//                     }
//
//                 </ul>
//                 <ErrorButton/>
//             </div>
//         </React.Fragment>
//     );
// };


export default class ItemDetails extends Component{

    // swapi = new SwapiService();

    state = {
        item: null,
        image: null,
        loading: true

    };

    componentDidMount() {
        this.updateItem();
        // this.interal = setInterval(this.updatePeople, 4000);
    }

    componentDidUpdate(prevProps) {
        if (this.props.itemId !== prevProps.itemId ||
            this.props.getData !== prevProps.getData ||
            this.props.getImage !== prevProps.getImage) {
            this.updateItem();
            // this.interal = setInterval(this.updatePeople, 4000);
            // clearInterval(this.interal);

        }
    }

    // onItemLoading = (item) => {
    //
    //     const {getImage} = this.props;
    //
    //    this.setState({
    //         item,
    //         image: getImage(item),
    //         loading: false
    //     });
    //     // console.log(JSON.stringify({getImage}, null, 3));
    // };


    updateItem() {
        const {itemId, getData, getImage} = this.props;
        console.log(JSON.stringify(getData, null, 3));

        if (!itemId) {
            return;
        }

        // getData(itemId)
        //     .then(this.onItemLoading)
        getData(itemId)
            .then((item) => {
                this.setState({
                    item,
                    image: getImage(item)
                });
            });
    };


    render() {

        // const {children} = this.props.children;

       const {item,loading, image} = this.state;

       if (!item) {
           return <span>Select item</span>
       }

       // if(!this.state.item) {
       //     return <Spinner/>
       // }
       //
       // const showResult = loading ? <Spinner/> : null;
       // const content = !loading ? <ItemView item={item} image={image} children={this.props.children}/> : null;

        // console.log(JSON.stringify({name}, null, 3));

        const {name} = item;

        return(

            <div className="item-details card">
                <React.Fragment>
                    <img className="item-image"
                         src={image}
                         alt="item"/>

                    <div className="card-body">
                        <h4>{name}</h4>
                        <ul className="list-group list-group-flush">

                            {
                                React.Children.map(this.props.children, (child) => {
                                    return React.cloneElement(child, { item });
                                })
                            }

                        </ul>
                        <ErrorButton/>
                    </div>
                </React.Fragment>
            </div>
        );
    }
    
}


