import  React, {Component} from 'react'
import {StarshipDetails, StarshipList} from "../SwComponents";
import {withRouter} from 'react-router-dom'

const StarshipPage = ({history}) => {

     return (
         <StarshipList onItemSelected={(itemId) => {
            const newPath = (itemId);
            history.push(newPath);
            }
         }/>
        );


};

export default withRouter(StarshipPage);