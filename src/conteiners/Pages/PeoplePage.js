import  React, {Component} from 'react'
import Row from "../Row";
import {PeopleDetails, PersonList} from "../SwComponents";
import ErrorController from "../ErrorController";
import {withRouter} from 'react-router-dom'

const PeoplePage = ({history, match}) => {

        const { id } = match.params;
        return (
            <ErrorController>
                <Row
                    left={ <PersonList onItemSelected={(id) => history.push(id)} />}
                    right={ <PeopleDetails itemId={id}/>}
                />
            </ErrorController>

        );


};

export default withRouter(PeoplePage);