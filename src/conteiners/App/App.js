
import React, {Component} from "react";

import Header from "../Header";
import RandomPlanet from "../RandomPlanet";
import ErrorButton from "../ErrorButton";
import "./app.css"
import Error from "../Errors";
import SwapiService from "../../swapiService/SwapiService";

import DummySwapiService from "../../swapiService/DummySwapiService";
import {SwapiProvider} from "../SwapiServiceContext";
import {LoginPage, PeoplePage, PlanetPage, SecretPage, StarshipPage} from "../Pages";
import PeopleScreen from "../WoukingPandI/PeopleContainer";
import ErrorController from "../ErrorController";
import {BrowserRouter as Router, Link, Route, Redirect, Switch} from 'react-router-dom'
import {StarshipDetails} from "../SwComponents";


export default class App extends Component{

    // swapi = new SwapiService();
    // swapi = new DummySwapiService();

    state = {
        onRandom: true,
        hasError: false,
        swapi: new SwapiService(),
        isLoggedIn: false
        // swapi: new SwapiService()

    };

    componentDidCatch(error, errorInfo) {
         // console.log('componentDidCatch()');
         this.setState({hasError:true})

    }

    onLogin = () => {
        this.setState({
            isLoggedIn: true
            }
        )
    };

    onOffRandomPlanet = () => {
        this.setState( {
            onRandom: !this.state.onRandom
        })
    };

    onChangeContext = () => {
        return (
            this.setState(({swapi}) => {
                    const Service = swapi instanceof SwapiService ? DummySwapiService : SwapiService;
                    return {
                        swapi: new Service()
                    }
                }

            )

        )
    };

    render() {

        const {isLoggedIn} = this.state;

        if (this.state.hasError) {
            return <Error/>
        }

        const {onRandom} = this.state;
        const visibleRandom = onRandom ? <RandomPlanet /> : null;

        // const peopleDetails = (
        //     <ItemDetails
        //         itemId={11}
        //         getData={getPeople}
        //         getImage={getPeopleImage}
        //     >
        //         <Record field = "gender" label = "Gender" />
        //         <Record field = "eyeColor" label = "Eye Color" />
        //
        //     </ItemDetails>
        // );
        // const starshipDetails = (
        //
        //         <ItemDetails
        //             itemId={11}
        //             getData={getStarship}
        //             getImage={getStarshipImage}
        //         >
        //             <Record field = "model" label = "Model" />
        //             <Record field = "consumables" label = "Consumables" />
        //             <Record field = "costInCredits" label = "Cost" />
        //         </ItemDetails>
        // );

        return(

            <ErrorController>
                <SwapiProvider value={this.state.swapi}>
                    <Router>
                      <Switch/>

                       <div className="stardb-app">
                        <Header onChangeContext={this.onChangeContext}/>
                        {visibleRandom}

                        <div className='row mb2 button-row' >
                            <button type="submit"
                                    className="toggle-planet btn btn-warning btn-lg"
                                    onClick={this.onOffRandomPlanet}
                            >On/Off Random planet
                            </button>
                            <ErrorButton />
                        </div>

                        {/*<PeopleScreen/>*/}
                        <Route path="/"  render={() => <h2>Welcome to StarDB</h2>}
                               exact/>
                        <Route path="/people/"   render={() => <h2>People</h2>}
                               exact/>
                        <Route path="/people/:id?" component={PeoplePage}/>
                        <Route path="/planets/" component={PlanetPage}/>
                        <Route path="/starships/" exact component={StarshipPage}/>
                        <Route path="/starships/:id"
                               render={({match}) => {
                                   const { id } = match.params;
                                   return <StarshipDetails itemId={id}/>
                               }}
                        />
                        <Route path="/login"
                               render={() => {
                                  return (
                                      <LoginPage
                                         isLoggenIn={isLoggedIn}
                                         onLogin={this.onLogin}
                                      />
                            );
                        }}/>
                        <Route path="/secret"
                               render={() => {
                                  return (
                                      <SecretPage isLoggedIn={isLoggedIn}/>
                                   )
                        }}/>
                           <Route render={() => <h2>Page not found</h2>}/>


                      </div>
                    <Switch/>
                  </Router>
                </SwapiProvider>
            </ErrorController>





        );
    }

}

