import  React from "react"
import "./errorStyle.css"

import robotFixer from "./robot-comprando-cesta_1048-3558.jpg"

const Error = () => {

    return(
      <div className="error-indicator">
          <img src={robotFixer} alt="robotFixer" className="img" />
          <span className="oops">Opps</span>
          <span>Something went wrong</span>
          <span>We already sent droids to fix it</span>
      </div>
    );
};

export default Error;